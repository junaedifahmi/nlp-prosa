## Class Defintion
import torch.nn as nn
import torch.nn.functional as F
import torch
import pickle
import sys

class FastText(nn.Module):
    def __init__(self, vocab_size, embedding_vec, output_dim, pad_idx, unk_idx):
        super().__init__()
        self.embedding = nn.Embedding(vocab_size, embedding_vec.shape[1], padding_idx=pad_idx)
        self.fc = nn.Linear(embedding_vec.shape[1], output_dim)
        # init embed weight
        self._init_embedding_weight(embedding_vec, pad_idx, unk_idx)
        
    def forward(self, text):
        text = self.embedding(text)
        text = text.permute(1, 0, 2)
        text = F.avg_pool2d(text, (text.shape[1], 1)).squeeze(1) 
        text = self.fc(text)
        return text
    
    def _init_embedding_weight(self, glove, pad_idx, unk_idx):
        self.embedding.weight.data.copy_(glove)
        self.embedding.weight.data[unk_idx] = torch.zeros(glove.shape[1])
        self.embedding.weight.data[pad_idx] = torch.zeros(glove.shape[1])


## Object load

with open('model/model.bin', 'rb') as f:
    model = pickle.load(f)

ftext = "model/text.bin"
with open(ftext, 'rb') as f:
    text = pickle.load(f)
    
flabel = "model/label.bin"
with open(flabel, 'rb') as f:
    label = pickle.load(f)

## Model weight loading
# model.load_state_dict(torch.load('model/model.pt'))
model.eval()

def predict_class(model, sentence, min_len = 4):
    tokenized = sentence.split()
    if len(tokenized) < min_len:
        tokenized += ['<pad>'] * (min_len - len(tokenized))
    indexed = [ text[t] for t in tokenized]
    tensor = torch.LongTensor(indexed)
    tensor = tensor.unsqueeze(1)
    preds = model(tensor)
    max_preds = preds.argmax(dim = 1)
    clas = label[max_preds.item()]
    return clas


if __name__ == "__main__":
    if len(sys.argv) > 1 :
        example = sys.argv[1]
    else:
        example = "lokasi strategis penasaran karena daerah situ rame trus parkirannya, entah yg ke hdl atau ke alas daun ya rame2 sama keluarga rasa biasa aja, mahal iya, sayur seuprit tempatnya juga biasa aja yang beda cuma makan pake alas daun pisang"

    clas = predict_class(model, example)
    print(" You entered \n", example)
    print("\nThe predicted class is", clas)

